(function () {
    'use strict';
    angular.module('RStudioBoggle')
        .constant('CONSTANTS', {
            NUM_ROWS: 4,
            NUM_COLS: 4,
            MIN_WORD_LENGTH: 3
        });
}());
