(function () {
    'use strict';
    angular.module('RStudioBoggle')
        .factory('EnglishDictionaryService', function ($http, toastr, CONSTANTS) {

            var wordMap,
                wordPrefixMap,
                isReady = false;
            var loadingMessage = toastr.info(null, 'loading dictionary',
                {
                    tapToDismiss: false,
                    timeOut: 0
                });
            // This dictionary is pretty bad but I found it from here:
            // https://github.com/dwyl/english-words/blob/master/words_dictionary.json
            var promise = $http.get(
                'features/dictionary/words_dictionary.json');

            promise.then(function (response) {
                wordMap = response.data;
                wordPrefixMap = {};
                angular.forEach(wordMap, function (tmp, word) {
                    if (word.length >= CONSTANTS.MIN_WORD_LENGTH) {
                        for (var pos = 0; pos < (word.length - 1); pos++) {
                            var key = word.substr(0, pos + 1).toLowerCase();
                            wordPrefixMap[key] = word;
                        }
                    }
                });
                toastr.clear(loadingMessage);
                isReady = true;
            });

            return {
                isAWord: isAWord,
                isAWordPrefix: isAWordPrefix,
                isDictionaryReady: isDictionaryReady
            };

            function isAWord(word) {
                if (word.length < CONSTANTS.MIN_WORD_LENGTH) {
                    return false;
                }
                return Boolean(wordMap[word.toLowerCase()]);
            }

            function isAWordPrefix(word) {
                return Boolean(wordPrefixMap[word.toLowerCase()]);
            }

            function isDictionaryReady() {
                return isReady;
            }
        });
}());
