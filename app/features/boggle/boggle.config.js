(function () {
    'use strict';

    angular
        .module('RStudioBoggle')
        .config(function ($stateProvider) {
            $stateProvider.state('boggle', {
                controller: 'BoggleController',
                name: 'boggle',
                params: {
                    timedOut: false, unauthorized: false, noOrganizations: false
                },
                templateUrl: 'features/boggle/boggle.html',
                url: '/'
            });
        });
}());
