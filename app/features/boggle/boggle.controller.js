(function () {
    'use strict';

    angular
        .module('RStudioBoggle')
        .controller('BoggleController', function (
            $log,
            $scope,
            $state,
            $window,
            toastr,
            BoggleModel
        ) {
            $scope.model = BoggleModel;
            $scope.solveBoggle = solveBoggle;
            BoggleModel.updateBoard();

            function solveBoggle() {
                var start = new Date().getTime();
                BoggleModel.solveBoggle();
                var time = new Date().getTime() - start;
                toastr.info(null, 'Solved Boggle in: ' + time + ' ms');
            }
        });
}());
