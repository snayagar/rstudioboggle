(function () {
    'use strict';

    angular
        .module('RStudioBoggle')
        .factory('BoggleModel', function (
            BaseModel,
            BoggleModelDefault,
            BoggleResource,
            EnglishDictionaryService,
            CONSTANTS
        ) {
            var model = new BaseModel(
                BoggleModelDefault.create(),
                BoggleResource
            );

            model.reset = reset;
            model.solveBoggle = solveBoggle;
            model.updateBoard = updateBoard;
            model.isBoardValid = isBoardValid;

            return model;

            function reset() {
                model.data = BoggleModelDefault.create();
            }

            function solveBoggle() {
                model.data.output = BoggleModelDefault.resetOutput();
                model.data.output.foundWords = [];
                updateBoard();
                findWordsByDebtFirstSearch();
            }

            // ported this JS code from c++ code located here:
            // https://www.geeksforgeeks.org/boggle-find-possible-words-board-characters/
            function findWordsByDebtFirstSearch() {

                var str = '';

                // Consider every character and look for all words
                // starting with this character
                for (var rowCount = 0; rowCount <
                CONSTANTS.NUM_ROWS; rowCount++) {
                    for (var colCount = 0; colCount <
                    CONSTANTS.NUM_COLS; colCount++) {
                        findWordsUtil(rowCount, colCount, str);
                    }
                }
            }

            // ported this JS code from c++ code located here:
            // https://www.geeksforgeeks.org/boggle-find-possible-words-board-characters/
            // Added a word prefix optimization to make the code faster.
            function findWordsUtil(rowCount, colCount, str) {
                // Mark current cell as visited and append current character
                // to str

                model.data.output.visited[rowCount][colCount] = true;
                str = str + model.data.output.board[rowCount][colCount];

                // If str is present in dictionary, then print it
                if (EnglishDictionaryService.isAWord(str)) {
                    model.data.output.foundWords.push(str);
                }

                // optimization this makes the dfs a ton quicker as we don't
                // walk down trees that have no valid word prefix
                if (EnglishDictionaryService.isAWordPrefix(str)) {
                    // Traverse 8 adjacent cells of boggle[i][j]
                    for (var row = rowCount - 1; row <= rowCount + 1 &&
                    row < CONSTANTS.NUM_ROWS; row++) {
                        for (var col = colCount - 1; col <= colCount + 1 &&
                        col < CONSTANTS.NUM_COLS; col++) {
                            if (row >= 0 && col >= 0 &&
                                !model.data.output.visited[row][col])
                                findWordsUtil(row, col, str);
                        }
                    }
                }

                // Erase current character from string and mark visited
                // of current cell as false
                str = str.substring(1, (str.length - 1));
                model.data.output.visited[rowCount][colCount] = false;
            }

            function isBoardValid() {
                return EnglishDictionaryService.isDictionaryReady() &&
                    !(!model.data.boggleInput ||
                        model.data.boggleInput.length <
                        model.data.requiredInputLength);
            }

            function updateBoard() {

                model.data.output.board = [];
                model.data.output.visited = [];
                for (var rowCount = 0; rowCount <
                CONSTANTS.NUM_ROWS; rowCount++) {
                    var row = [];
                    var visitedRow = [];
                    for (var colCount = rowCount *
                        CONSTANTS.NUM_COLS; colCount <
                         (rowCount + 1) * CONSTANTS.NUM_COLS; colCount++) {
                        if (!model.data.boggleInput ||
                            model.data.boggleInput.length < colCount) {
                            row.push('');
                        } else {
                            row.push(model.data.boggleInput[colCount]);
                        }

                        visitedRow.push(false);
                    }
                    model.data.output.board.push(row);
                    model.data.output.visited.push(visitedRow);
                }
            }
        });
}());

