(function () {
    'use strict';

    angular
        .module('RStudioBoggle')
        .factory('BoggleModelDefault',
            function (CONSTANTS) {
                var output = {
                    board: null,
                    visited: null,
                    foundWords: null
                };

                var defaultValues = {
                    boggleInput: '',
                    requiredInputLength: CONSTANTS.NUM_ROWS *
                    CONSTANTS.NUM_COLS,
                    output: output
                };

                function create() {
                    return angular.copy(defaultValues);
                }

                function resetOutput() {
                    return angular.copy(output);
                }

                return {
                    create: create,
                    resetOutput: resetOutput
                };
            });
}());
