(function () {
    'use strict';

    angular
        .module('RStudioBoggle')
        .factory('BoggleResource', function (
            BaseResource
        ) {
            var actions,
                endpoint = '',
                params = {};

            actions = {
                fetchDictionary: {
                    method: 'GET',
                    url: ''
                }
            };
            return new BaseResource(endpoint, params, actions);
        });
}());
