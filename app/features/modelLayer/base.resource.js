(function () {
    'use strict';

    /**
     * @ngdoc Service
     * @name DefaultResource
     *
     * @description
     * An abstract resource used for api access. This provide default routes
     * and any http headers required by the API. This was object designed to be
     * used by model resource object as a base class.
     *
     */
    angular
        .module('modelLayer')
        .provider('BaseResource', function BaseResource(lodash) {
            this.baseConfig = null;
            var that = this;
            this.$get = function ($resource, $log) {

                if (that.baseConfig === null) {
                    $log.error(
                        'BaseResource: baseConfig has not been specified');
                    return;
                }

                var baseParams = {};
                var baseHeaders = that.defaultHeaders || {};

                var baseActions = {
                    get: {
                        method: 'GET',
                        responseType: 'json',
                        headers: baseHeaders,
                        transformRequest: [],
                        transformResponse: []
                    },
                    save: {
                        method: 'POST',
                        responseType: 'json',
                        headers: baseHeaders,
                        transformRequest: [],
                        transformResponse: []
                    },
                    query: {
                        method: 'GET',
                        responseType: 'json',
                        headers: baseHeaders,
                        transformRequest: [],
                        transformResponse: [],
                        isArray: true
                    },
                    delete: {
                        method: 'DELETE',
                        responseType: 'json',
                        headers: baseHeaders,
                        transformRequest: [],
                        transformResponse: []
                    }
                };

                function getFinalURL(resourceURL) {
                    // HACK this is required as injector and $interpolate are
                    // not available in this provider
                    var $interpolate = angular.element(document)
                        .injector().get('$interpolate');
                    return $interpolate(resourceURL)(that.baseConfig);
                }

                // This function merges the provided actions with the base ones
                // so that all requests and responses have the correct urls and
                // transforms added.
                var getAPIAction = function (action, key) {
                    var apiAction = angular.copy(baseActions[key]);

                    if (!apiAction) {
                        apiAction = angular.copy(action);
                        apiAction.headers = baseHeaders;
                        apiAction.responseType = 'json';
                        apiAction.transformRequest = [];
                        apiAction.transformResponse = [];
                    }

                    if (action.url) {
                        if (action.url.indexOf('webroot') > -1) {
                            apiAction.url = action.url.replace('webroot', '');
                        } else if (action.url.indexOf('http') > -1) {
                            apiAction.url = action.url;
                        } else {
                            apiAction.url = getFinalURL(apiAction.url);
                        }
                    }

                    if (action.transformRequest) {
                        apiAction.transformRequest.push(
                            action.transformRequest);
                    }

                    if (action.transformResponse) {
                        apiAction.transformResponse.push(
                            action.transformResponse);
                    }

                    if (action.headers) {
                        apiAction.headers = action.headers;
                    }

                    if (action.isArray) {
                        apiAction.isArray = action.isArray;
                    }

                    var interceptor = {};

                    if (angular.isFunction(action.responseErrorHandler)) {
                        interceptor.responseError =
                            action.responseErrorHandler;
                    }

                    if (angular.isFunction(action.requestHandler)) {
                        // hack there is no request interceptor so have to use
                        // request transformation hook instead
                        apiAction.transformRequest.push(function (model) {
                            action.requestHandler();
                            return model;
                        });
                    }

                    if (angular.isFunction(action.responseHandler)) {
                        interceptor.response =
                            action.responseHandler;
                    }

                    if (!lodash.isEmpty(interceptor)) {
                        apiAction.interceptor = interceptor;
                    }

                    delete apiAction.requestHandler;
                    delete apiAction.responseHandler;
                    delete apiAction.responseErrorHandler;

                    return apiAction;
                };

                return function (endpoint, params, actions) {
                    var url = getFinalURL(endpoint);
                    var actionsToUse = angular.copy(baseActions);
                    if (actions) {
                        lodash.forEach(actions, function (action, key) {
                            actionsToUse[key] = getAPIAction(action, key);
                        });
                    }

                    var p = lodash.extend(params, baseParams);
                    return $resource(url, p, actionsToUse);
                };
            };
        });
}());
