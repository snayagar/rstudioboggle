(function () {
    'use strict';

    angular
        .module('modelLayer', ['ngResource', 'ngLodash']);
}());
