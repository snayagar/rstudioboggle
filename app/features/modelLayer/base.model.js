(function () {
    'use strict';

    /**
     * @ngdoc Service
     * @name BaseModel
     *
     * @description
     * An abstract model used for data-binding and API access
     */
    angular
        .module('modelLayer')
        .factory('BaseModel', function ($rootScope, lodash) {

            function BaseModel(defaults, resource) {
                this.data = defaults;
                this.resource = resource;

                // handle errors and assign model data
                var that = this;
                $rootScope.$watch(
                    function () {
                        return that.data;
                    },
                    function (newValue) {
                        if (newValue.$promise) {
                            newValue.$promise.catch(function (response) {
                                if (response.data) {
                                    lodash.extend(that.data, response.data);
                                }
                            });
                        }
                    }
                );
            }

            BaseModel.prototype.get = function (options) {
                this.data = this.resource.get(options);
            };

            BaseModel.prototype.save = function (options) {
                this.data = this.resource.save(options, this.data);
            };

            BaseModel.prototype.query = function (options) {
                this.data = this.resource.query(options);
            };

            BaseModel.prototype.delete = function (options) {
                this.data = this.resource.delete(options);
            };

            return BaseModel;
        });
}());
