(function () {
    'use strict';

    angular
        .module('RStudioBoggle', [
            'ENV',
            'ui.router',
            'LocalStorageModule',
            'ngResource',
            'ngCookies',
            'ngLodash',
            'toastr',
            'modelLayer'
        ])
        .config(function (
            $stateProvider,
            $locationProvider,
            $httpProvider,
            $cookiesProvider,
            $urlRouterProvider,
            $qProvider,
            BaseResourceProvider,
            ENV
        ) {

            BaseResourceProvider.baseConfig = ENV;
            $cookiesProvider.defaults.path = '/';

            $urlRouterProvider.when('', '/');

            // eats unhandled exceptions
            // TODO added a global exception handler
            $qProvider.errorOnUnhandledRejections(false);

            $locationProvider.html5Mode({
                enabled: true,
                requireBase: false
            });
        })
        .run(function (
            ENV,
            CONSTANTS,
            $rootScope,
            $window
        ) {
            $rootScope.ENV = ENV;
            $rootScope.CONSTANTS = CONSTANTS;
            $rootScope.$on('$stateChangeSuccess', function (event, toState) {
                $window.scrollTo(0, 0);
                $rootScope.currentState = toState.name;
            });
        });
}());
