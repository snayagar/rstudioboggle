// Generated on 2017-03-06 using generator-angular 0.16.0
'use strict';

// # Globbing
// for performance reasons we're only matching one level down:
// 'test/spec/{,*/}*.js'
// use this if you want to recursively match all subfolders:
// 'test/spec/**/*.js'

module.exports = function (grunt) {

    // Time how long tasks take. Can help when optimizing build times
    require('time-grunt')(grunt);

    var modRewrite = require('connect-modrewrite');
    // Automatically load required Grunt tasks
    require('jit-grunt')(grunt, {
        useminPrepare: 'grunt-usemin',
        ngtemplates: 'grunt-angular-templates',
        eslint: 'gruntify-eslint',
        ngconstant: 'grunt-ng-constant',
        configureProxies: 'grunt-connect-proxy'
    });

    // Configurable paths for the application
    var appConfig = {
        app: 'app',
        dist: 'dist'
    };

    // Define the configuration for all the tasks
    grunt.initConfig({

        // Project settings
        yeoman: appConfig,

        // Watches files for changes and runs tasks based on the changed files
        watch: {
            js: {
                files: ['<%= yeoman.app %>/scripts/{,*/}*.js'],
                tasks: ['newer:eslint'],
                options: {
                    livereload: '<%= connect.options.livereload %>'
                }
            },
            jsTest: {
                files: ['test/spec/{,*/}*.js'],
                tasks: ['newer:jshint:test', 'newer:jscs:test', 'karma']
            },
            styles: {
                files: [
                    '<%= yeoman.app %>/styles/**/*.scss',
                    '<%= yeoman.app %>/features/**/*.scss'
                ],
                tasks: ['newer:copy:styles', 'sass:dist']
            },
            gruntfile: {
                files: ['Gruntfile.js']
            },
            livereload: {
                options: {
                    livereload: '<%= connect.options.livereload %>'
                },
                files: [
                    '<%= yeoman.app %>/**/*.html',
                    '.tmp/styles/{,*/}*.css',
                    '<%= yeoman.app %>/images/**/*.{png,jpg,jpeg,gif,webp,svg}'
                ]
            }
        },

        // The actual grunt server settings
        connect: {
            options: {
                port: 8000,
                // Change this to '0.0.0.0' to access the server from outside.
                hostname: 'localhost'
            },
            livereload: {
                options: {
                    open: false,
                    middleware: function (connect) {

                        var middlewares = [];

                        // Redirect request to admin requests to the correct
                        // path
                        middlewares.push(
                            modRewrite(['^/admin$ /admin/ [R]'])
                        );

                        // Setup the proxy
                        middlewares.push([require(
                            'grunt-connect-proxy/lib/utils').proxyRequest]);

                        // Redirect requests from admin requests to root
                        middlewares.push(
                            modRewrite(['^/dashboard/lead/(.*) /lead/$1 [R]'])
                        );

                        // Angular html5 mode support
                        middlewares.push(
                            modRewrite(['!\\.html|\\.js|\\.svg|' +
                            '\\.css|\\.scss.*|\\.woff.*|\\.gif.*|\\.png$ ' +
                            '/index.html [L]'])
                        );

                        var staticRoutes = [
                            connect.static('.tmp'),
                            connect().use(
                                '/bower_components',
                                connect.static('./bower_components')
                            ),
                            connect().use(
                                '/node_modules',
                                connect.static('./node_modules')
                            ),
                            connect().use(
                                '/app/styles',
                                connect.static('./app/styles')
                            ),
                            connect.static(appConfig.app)
                        ];


                        return middlewares.concat(staticRoutes);
                    }
                }
            },
            test: {
                options: {
                    port: 9902,
                    middleware: function (connect) {
                        return [
                            connect.static('.tmp'),
                            connect.static('test'),
                            connect().use(
                                '/bower_components',
                                connect.static('./bower_components')
                            ),
                            connect().use(
                                '/node_modules',
                                connect.static('./node_modules')
                            ),
                            connect.static(appConfig.app)
                        ];
                    }
                }
            },
            dist: {
                options: {
                    open: true,
                    base: '<%= yeoman.dist %>',
                    middleware: function (connect) {
                        var middlewares = [];

                        // Redirect request to admin requests to the correct
                        // path
                        middlewares.push(
                            modRewrite(['^/admin$ /admin/ [R]'])
                        );

                        // Setup the proxy
                        middlewares.push([require(
                            'grunt-connect-proxy/lib/utils').proxyRequest]);

                        // Redirect requests from admin requests to root
                        middlewares.push(
                            modRewrite(['^/dashboard/lead/(.*) /lead/$1 [R]'])
                        );

                        // Angular html5 mode support
                        middlewares.push(
                            modRewrite(['!\\.html|\\.js|\\.svg|' +
                            '\\.css|\\.scss.*|\\.woff.*|\\.gif.*|\\.png$ ' +
                            '/index.html [L]'])
                        );

                        middlewares.push(connect.static(appConfig.dist));
                        return middlewares;
                    }
                }
            }
        },

        eslint: {
            src: [
                '<%= yeoman.app %>/**/*.js'
            ]
        },

        // Empties folders to start fresh
        clean: {
            dist: {
                files: [{
                    dot: true,
                    src: [
                        '.tmp',
                        '<%= yeoman.dist %>/{,*/}*',
                        '!<%= yeoman.dist %>/.git{,*/}*'
                    ]
                }]
            },
            server: '.tmp'
        },

        sass: {
            dist: {
                options: {
                    outputStyle: 'compressed',
                    sourceMap: true
                },
                files: [{
                    expand: true,
                    cwd: '<%= yeoman.app %>',
                    src: ['**/*.scss'],
                    dest: '<%= yeoman.app %>/',
                    ext: '.css'
                }]
            }
        },

        ngconstant: {
            options: {
                name: 'ENV',
                wrap: [
                    '(function () {',
                    '    \'use strict\';',
                    '    /* eslint-disable */',
                    '    {%= __ngModule %}',
                    '}());\n'
                ].join('\n')
            },
            ENV: {
                options: {
                    dest: '<%= yeoman.app %>/env.constant.js'
                },
                constants: {
                    ENV: {
                        APP: 'RStudioBoggle',
                        GOOGLE_ANALYTICS_CODE: '',
                        BUILD_NUMBER: process.env.BITBUCKET_BUILD_NUMBER || 'localBuild',
                        GIT_COMMIT: process.env.BITBUCKET_COMMIT || 'localBranch'
                    }
                }
            }
        },

        // Renames files for browser caching purposes
        filerev: {
            options: {
                process: function (basename, name, extension) {
                    var buildNumber = process.env.BITBUCKET_BUILD_NUMBER || 'localBuild';
                    return basename + '.' + buildNumber + '.' + extension;
                }
            },
            dist: {
                src: [
                    '<%= yeoman.dist %>/scripts/{,*/}*.js',
                    '<%= yeoman.dist %>/styles/{,*/}*.css',
                    '<%= yeoman.dist %>/images/{,*/}*.{png,jpg,jpeg,gif,webp,svg}',
                    '<%= yeoman.dist %>/styles/fonts/*'
                ]
            }
        },

        // Reads HTML for usemin blocks to enable smart builds that
        // automatically concat, minify and revision files. Creates
        // configurations in memory so additional tasks can operate on them
        useminPrepare: {
            html: '<%= yeoman.app %>/index.html',
            options: {
                dest: '<%= yeoman.dist %>',
                flow: {
                    html: {
                        steps: {
                            js: ['concat', 'uglifyjs'],
                            css: ['cssmin']
                        },
                        post: {}
                    }
                }
            }
        },

        // Performs rewrites based on filerev and the useminPrepare
        // configuration
        usemin: {
            html: ['<%= yeoman.dist %>/**/*.html'],
            css: ['<%= yeoman.dist %>/styles/*.css'],
            js: ['<%= yeoman.dist %>/scripts/*.js'],
            options: {
                assetsDirs: [
                    '<%= yeoman.dist %>',
                    '<%= yeoman.dist %>/images',
                    '<%= yeoman.dist %>/styles'
                ],
                patterns: {
                    js: [[/(images\/[^''""]*\.(png|jpg|jpeg|gif|webp|svg))/g,
                        'Replacing references to images']]
                }
            }
        },

        // The following *-min tasks will produce minified files in the dist
        // folder By default, your `index.html`'s <!-- Usemin block --> will
        // take care of minification. These next options are pre-configured if
        // you do not wish to use the Usemin blocks. cssmin: { dist: { files: {
        // '<%= yeoman.dist %>/styles/main.css': [ '.tmp/styles/{,*/}*.css' ] }
        // } }, uglify: { dist: { files: { '<%= yeoman.dist
        // %>/scripts/scripts.js': [ '<%= yeoman.dist %>/scripts/scripts.js' ]
        // } } }, concat: { dist: {} },

        imagemin: {
            dist: {
                files: [{
                    expand: true,
                    cwd: '<%= yeoman.app %>/images',
                    src: '{,*/}*.{png,jpg,jpeg,gif}',
                    dest: '<%= yeoman.dist %>/images'
                }]
            }
        },

        svgmin: {
            dist: {
                files: [{
                    expand: true,
                    cwd: '<%= yeoman.app %>/images',
                    src: '{,*/}*.svg',
                    dest: '<%= yeoman.dist %>/images'
                }]
            }
        },

        htmlmin: {
            dist: {
                options: {
                    collapseWhitespace: true,
                    conservativeCollapse: true,
                    collapseBooleanAttributes: true,
                    removeCommentsFromCDATA: true
                },
                files: [{
                    expand: true,
                    cwd: '<%= yeoman.dist %>',
                    src: ['*.html'],
                    dest: '<%= yeoman.dist %>'
                }]
            }
        },

        ngtemplates: {
            dist: {
                options: {
                    module: 'RStudioBoggle',
                    htmlmin: '<%= htmlmin.dist.options %>',
                    usemin: 'scripts/scripts.js'
                },
                cwd: '<%= yeoman.app %>',
                src: '[views/**/*.html,features/**/*.html]',
                dest: '.tmp/templateCache.js'
            }
        },

        // ng-annotate tries to make the code safe for minification
        // automatically by using the Angular long form for dependency
        // injection.
        ngAnnotate: {
            dist: {
                files: [{
                    expand: true,
                    cwd: '.tmp/concat/scripts',
                    src: '*.js',
                    dest: '.tmp/concat/scripts'
                }]
            }
        },

        // Copies remaining files to places other tasks can use
        copy: {
            dist: {
                files: [{
                    expand: true,
                    dot: true,
                    cwd: '<%= yeoman.app %>',
                    dest: '<%= yeoman.dist %>',
                    src: [
                        '*.{ico,png,txt}',
                        '*.html',
                        'features/**/*.html',
                        'features/**/*.json',
                        'views/**/*.html',
                        'images/**/*.{webp}',
                        'fonts/**/*.*'
                    ]
                }, {
                    expand: true,
                    cwd: '.tmp/images',
                    dest: '<%= yeoman.dist %>/images',
                    src: ['generated/*']
                }, {
                    expand: true,
                    cwd: 'node_modules/bootstrap/dist',
                    src: 'fonts/*',
                    dest: '<%= yeoman.dist %>'
                }]
            },
            styles: {
                expand: true,
                cwd: '<%= yeoman.app %>/styles',
                dest: '.tmp/styles/',
                src: '**/*.css'
            }
        },

        // Run some tasks in parallel to speed up the build process
        concurrent: {
            server: [
                'copy:styles'
            ],
            test: [
                'copy:styles'
            ],
            dist: [
                'copy:styles',
                'imagemin',
                'svgmin'
            ]
        },

        // Test settings
        karma: {
            unit: {
                configFile: 'karma.conf.js',
                singleRun: true
            }
        },
        'string-replace': {
            dist: {
                files: [{
                    expand: true,
                    cwd: 'dist/',
                    src: 'index.html',
                    dest: 'dist/'
                }],
                options: {
                    replacements: [
                        {
                            pattern: '<base href="/">',
                            replacement: '<base href="' +
                            (grunt.option('siteRoot') ||
                                '/') + '">'
                        }
                    ]
                }
            }
        }
    });

    grunt.registerTask('serve', 'Compile then start a connect web server',
        function (target) {
            if (target === 'dist') {
                return grunt.task.run([
                    'configureProxies:server',
                    'connect:dist:keepalive'
                ]);
            }

            grunt.task.run([
                'clean:server',
                'configureProxies:server',
                'concurrent:server',
                'connect:livereload',
                'watch'
            ]);
        });

    grunt.registerTask('test', [
        'clean:server',
        'concurrent:test',
        'sass',
        'connect:test',
        'karma'
    ]);

    grunt.registerTask('build', [
        'yarn',
        'clean:dist',
        'ngconstant:ENV',
        'useminPrepare',
        'concurrent:dist',
        'sass',
        'ngtemplates',
        'concat',
        'ngAnnotate',
        'copy:dist',
        'cssmin',
        'uglify',
        'filerev',
        'usemin',
        'htmlmin',
        'string-replace:dist'
    ]);

    grunt.registerTask('default', [
        'newer:eslint',
        'test',
        'build'
    ]);

    grunt.registerTask('yarn', 'install the yarn dependencies', function () {
        var exec = require('child_process').exec;
        var cb = this.async();
        exec('yarn', {cwd: './node_modules'},
            function (err, stdout) {
                console.log(stdout);
                cb();
            });
    });
};
